function ngayMai() {

    var day = document.getElementById("ngay").value;
    var month = document.getElementById("thang").value;
    var year = document.getElementById("nam").value;

    switch (month) {
        case "1": {

            if (day == 31) {
                document.getElementById("result").innerHTML = `<p>1/${+month + (1)}/${year}</p>`
            }
            else {
                document.getElementById("result").innerHTML = `<p>${+day + 1}/${month}/${year}</p>`
            }

        }
            break;
        case "2": {

            if (year % 4 == 0 && year % 100 != 0 || year % 100 == 0) {
                if (day == 29) {
                    document.getElementById("result").innerHTML = `<p>1/${+month + 1}/${year}</p>`
                }
                else {
                    document.getElementById("result").innerHTML = `<p>${+day + 1}/${month}/${year}</p>`
                }
            }
            else {
                if (day == 28) {
                    document.getElementById("result").innerHTML = `<p>1/${+month + (1)}/${year}</p>`
                }
                else {
                    document.getElementById("result").innerHTML = `<p>${+day + 1}/${month}/${year}</p>`
                }
            }

        }
            break;
        case "3": {

            if (day == 31) {
                document.getElementById("result").innerHTML = `<p>1/${+month + (1)}/${year}</p>`
            }
            else {
                document.getElementById("result").innerHTML = `<p>${+day + 1}/${month}/${year}</p>`
            }

        }
            break;
        case "4": {

            if (day == 30) {
                document.getElementById("result").innerHTML = `<p>1/${+month + (1)}/${year}</p>`
            }
            else {
                document.getElementById("result").innerHTML = `<p>${+day + 1}/${month}/${year}</p>`
            }

        }
            break;
        case "5": {

            if (day == 31) {
                document.getElementById("result").innerHTML = `<p>1/${+month + (1)}/${year}</p>`
            }
            else {
                document.getElementById("result").innerHTML = `<p>${+day + 1}/${month}/${year}</p>`
            }

        }
            break;
        case "6": {

            if (day == 30) {
                document.getElementById("result").innerHTML = `<p>1/${+month + (1)}/${year}</p>`
            }
            else {
                document.getElementById("result").innerHTML = `<p>${+day + 1}/${month}/${year}</p>`
            }

        }
            break;
        case "7": {

            if (day == 31) {
                document.getElementById("result").innerHTML = `<p>1/${+month + (1)}/${year}</p>`
            }
            else {
                document.getElementById("result").innerHTML = `<p>${+day + 1}/${month}/${year}</p>`
            }

        }
            break;
        case "8": {

            if (day == 31) {
                document.getElementById("result").innerHTML = `<p>1/${+month + (1)}/${year}</p>`
            }
            else {
                document.getElementById("result").innerHTML = `<p>${+day + 1}/${month}/${year}</p>`
            }

        }
            break;
        case "9": {

            if (day == 30) {
                document.getElementById("result").innerHTML = `<p>1/${+month + (1)}/${year}</p>`
            }
            else {
                document.getElementById("result").innerHTML = `<p>${+day + 1}/${month}/${year}</p>`
            }

        }
            break;
        case "10": {

            if (day == 31) {
                document.getElementById("result").innerHTML = `<p>1/${+month + (1)}/${year}</p>`
            }
            else {
                document.getElementById("result").innerHTML = `<p>${+day + 1}/${month}/${year}</p>`
            }

        }
            break;
        case "11": {

            if (day == 30) {
                document.getElementById("result").innerHTML = `<p>1/${+month + (1)}/${year}</p>`
            }
            else {
                document.getElementById("result").innerHTML = `<p>${+day + 1}/${month}/${year}</p>`
            }

        }
            break;
        case "12": {

            if (day == 31) {
                document.getElementById("result").innerHTML = `<p>1/1/${+year + 1}</p>`
            }
            else {
                document.getElementById("result").innerHTML = `<p>${+day + 1}/${month}/${year}</p>`
            }

        }
            break;
        default: {
            document.getElementById("result").innerHTML = `<p>ngày tháng năm ko hợp lệ</p>`
        }
    }
}
function ngayHomQua() {
    var day = document.getElementById("ngay").value;
    var month = document.getElementById("thang").value;
    var year = document.getElementById("nam").value;

    if (day == 1) {
        switch (+month - 1) {
            case 4:
            case 6:
            case 9:
            case 11:
                day = 30;
                break;
            case 2:
                day = 28 + ((year % 4 == 0 && year % 100) || (year % 400 == 0));
                break;
            default:
                day = 31;
        }
        if (month == 1) {
            month = 12;
            year--;
        }
        else {
            month--;
        }
    }
    else {
        day--;
    }
    document.getElementById("result").innerHTML = `<p>${day}/${month}/${year}</p>`
}
// bài 2:
function tinhNgay() {

    var thang = document.getElementById("month").value;
    var nam = document.getElementById("year").value;
    if (thang <= 1 || thang <= 12) {
        switch (thang) {
            case '1': case '3': case '5': case '7': case '8': case '10': case '12':
                document.getElementById("result1").innerHTML = `tháng ${thang} có 31 ngày`;
                break;
            case '4': case '6': case '9': case '11':
                document.getElementById("result1").innerHTML = `tháng ${thang} có 30 ngày`;
                break;
            case '2': {
                if (nam % 4 == 0) {
                    document.getElementById("result1").innerHTML = `tháng ${thang} có 29 ngày`;
                }
                else {
                    document.getElementById("result1").innerHTML = `tháng ${thang} có 28 ngày`;
                }
            }
                break;
        }

    }
    else {
        document.getElementById('result1').innerHTML = `<p>ko tồn tại tháng này</p>`
    }
}
// bài 3:
// c2:
// function docSo(){
//     var numberObj = {
//         "1": 'Một',
//         "2": 'Hai',
//         "3": 'Ba',
//         "4": 'Bốn',
//         "5": 'Năm',
//         "6": 'Sáu',
//         "7": 'Bảy',
//         "8": 'Tám',
//         "9": 'Chín',
//     }

//     const numversValues= +document.getElementById("so-co-3-chu-so").value;

//     if(numversValues < 100 || numversValues > 999) {
//         document.getElementById("result2").innerHTML=`<p>vui lòng nhập số lớn   hơn 99:</p>`
//     } else {
//       var  unitNumber = numversValues % 10 ;
//       var  tenNumber = Math.floor(numversValues / 10) % 10;
//       const  hundredNumber = Math.floor(numversValues / 100);


//       var tenString = '';
//       var unitString = '';

//       // check unit number
//       if(unitNumber === 0) {
//         unitString = ''
//       } else if(unitNumber >  0 && unitNumber !== 1) {
//         unitString = numberObj[unitNumber]
//       } else if(tenNumber === 0 || tenNumber === 1) {
//         unitString = ' một '
//       } else if(tenNumber > 0 && tenNumber !== 1) {
//         unitString = ' mốt '
//       }


//       // check ten string
//       if(tenNumber === 0 && unitNumber > 0) {
//         tenString = ' lẻ '
//         tenNumber = ''
//       } else if(tenNumber >  0 && tenNumber !== 1 && unitNumber === 0 ) {
//         tenString = numberObj[tenNumber].toLowerCase();
//         tenNumber = ''
//       } else if(tenNumber === 0 && unitNumber === 0) {
//         tenString = ''
//         tenNumber = ''
//       } else if(tenNumber > 0 && tenNumber !== 1 && unitNumber > 0) {
//         tenString = numberObj[tenNumber].toLowerCase() + ' mươi '
//       } else if(tenNumber > 0 && tenNumber === 1 && unitNumber === 0) {
//         tenString = ' mười '
//       } 

//       else if(tenNumber > 0 && tenNumber === 1 && unitNumber > 0) {
//         tenString = ' mười '
//       } 

//       // Print string
//       console.log('ten', tenNumber)
//       document.getElementById("result2").innerHTML= `${numberObj[hundredNumber] + ' trăm ' + tenString + unitString.toLowerCase() }`

//       console.log('tenNumber', tenNumber) 
//       console.log('hundredNumber', unitNumber) 
//       console.log('unitNumber', unitNumber) 
//     }
// c1:
function docSo() {
    var soCoBaChuSo = document.getElementById("so-co-3-chu-so").value * 1;
    var hangDonVi = soCoBaChuSo % 10;
    var hangChuc = Math.floor(soCoBaChuSo / 10) % 10;
    var hangTram = Math.floor(soCoBaChuSo / 100);
    var unitNumber
    var dozensNumber
    var hundredsNumber
    var odd
    if(soCoBaChuSo<100||soCoBaChuSo>999){
        document.getElementById("result2").innerHTML=`<p>số ko hợp lệ vui lòng nhập lại</p>`
    }
    else{
        switch(hangTram){
            case 1:{
                hundredsNumber='một trăm'
                break;
            }
            case 2:{
                hundredsNumber='hai trăm'
                break;
            }
            case 3:{
                hundredsNumber='ba trăm'
                break;
            }
            case 4:{
                hundredsNumber='bốn trăm'
                break;
            }
            case 5:{
                hundredsNumber='năm trăm'
                break;
            }
            case 6:{
                hundredsNumber='sáu trăm'
                break;
            }
            case 7:{
                hundredsNumber='bảy trăm'
                break;
            }
            case 8:{
                hundredsNumber='tám trăm'
                break;
            }
            case 9:{
                hundredsNumber='chín trăm'
                break;
            }
        }
        if (hangChuc%10==0&&hangDonVi!=0){
            odd = document.getElementById("result2").innerHTML=`<p>lẻ</p>`
        }
        switch(hangChuc){
            case 1:{
                dozensNumber='mười'
                break;
            }
            case 2:{
                dozensNumber='hai mươi'
                break;
            }
            case 3:{
                dozensNumber='ba mươi'
                break;
            }
            case 4:{
                dozensNumber='bốn mươi'
                break;
            }
            case 5:{
                dozensNumber='năm mươi'
                break;
            }
            case 6:{
                dozensNumber='sáu mươi'
                break;
            }
            case 7:{
                dozensNumber='bảy mươi'
                break;
            }
            case 8:{
                dozensNumber='tám mươi'
                break;
            }
            case 9:{
                dozensNumber='chín chục'
                break;
            }
        }
        switch(hangDonVi){
            case 1:{
                unitNumber='Một'
                break;
            }
            case 2:{
                unitNumber='hai'
                break;
            }
            case 3:{
                unitNumber='ba'
                break;
            }
            case 4:{
                unitNumber='bốn'
                break;
            }
            case 5:{
                unitNumber='năm'
                break;
            }
            case 6:{
                unitNumber='sáu'
                break;
            }
            case 7:{
                unitNumber='bảy'
                break;
            }
            case 8:{
                unitNumber='tám'
                break;
            }
            case 9:{
                unitNumber='chín'
                break;
            }
        }
    }
    if(hangChuc%10==0&&hangDonVi!=0){
        document.getElementById("result2").innerHTML=`${hundredsNumber} ${odd} ${unitNumber}`
    }
    else if(hangDonVi==0){
        document.getElementById("result2").innerHTML=`${hundredsNumber} ${dozensNumber}`
    }
    else{
        document.getElementById("result2").innerHTML=`${hundredsNumber} ${dozensNumber} ${unitNumber}`
    }
    
}
function timSvXaTruongNhat(){
    var nameS1=document.getElementById("name1").value
    var nameS2=document.getElementById("name2").value
    var nameS3=document.getElementById("name3").value
    var x1=document.getElementById("x1").value*1
    var y1=document.getElementById("y1").value*1
    var x2=document.getElementById("x2").value*1
    var y2=document.getElementById("y2").value*1 
    var x3=document.getElementById("x3").value*1
    var y3=document.getElementById("y3").value*1 
    var x4=document.getElementById("x4").value*1
    var y4=document.getElementById("y4").value*1
    var Square1= (x4-x1)*(x4-x1)+(y4-y1)*(y4-y1)
    var Square2= (x4-x2)*(x4-x2)+(y4-y2)*(y4-y2)
    var Square3= (x4-x3)*(x4-x3)+(y4-y3)*(y4-y3)

    var d1= Math.sqrt(Square1)
    var d2= Math.sqrt(Square2)
    var d3= Math.sqrt(Square3)  
    var resultName
    if(d1<d2){  
       d2<d3?resultName=nameS3:resultName=nameS2
    }else{
        d1>d3?resultName=nameS1:resultName=nameS3
    } 
    document.getElementById("result3").innerHTML=` Sinh viên xa trường nhất là: ${resultName}`

}







   
       


